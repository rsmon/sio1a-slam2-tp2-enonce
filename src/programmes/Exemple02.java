package programmes;

import        gestionpersonnel.Salarie;
import static gestionpersonnel.Entreprise.getSalarieDeNumero;

public class Exemple02 {

    public  void executer() {
      
        Long numsal=110L;
        
        Salarie sal= getSalarieDeNumero(numsal);
        
        System.out.println();
        
        if (sal!=null){ System.out.printf("%3d %-15s\n", sal.getId(),sal.getNom()); }
        else          { System.out.printf("Il n'y a pas de Salarié numéro %3d \n", numsal); }
        
        System.out.println();
    }
}


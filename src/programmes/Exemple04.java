package programmes;

import        gestionpersonnel.Salarie;
import static gestionpersonnel.Entreprise.getTousLesPoles;
import static gestionpersonnel.Tris.trierParNomPrenom;
import        gestionpersonnel.Pole;
import static utilitaires.UtilDate.aujourdhuiChaine;

public class Exemple04 {

    private final String titre="\nListe de salariés par Pôles à la date du: %-8s \n\n";
    
    public  void executer() {
      
       System.out.printf( titre, aujourdhuiChaine());
       
       for(Pole pole : getTousLesPoles()){
       
          System.out.println(pole.getNomPole()); System.out.println();
            
          for( Salarie sal:  trierParNomPrenom (pole.getLesSalaries()) ){
        
             sal.afficher();System.out.println();
          }
        
          System.out.println();
        
       }
       System.out.println();
    }
}


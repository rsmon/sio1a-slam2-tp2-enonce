
package gestionpersonnel;

import java.util.LinkedList;
import java.util.List;

public class Pole {
  
    //<editor-fold defaultstate="collapsed" desc="Attributs privés">
    
    private String codePole;
    private String nomPole;
    
    // Attribut navigationnel
    private List<Salarie> lesSalaries = new LinkedList();
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Constructeurs">
    
    public Pole() {}
    
    public Pole(String codePole, String nomPole) {
     
        this.codePole = codePole;
        this.nomPole  = nomPole;
     
    }
    
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Getters et Setters">
    
    public String getCodePole() {
        return codePole;
    }
    
    public String getNomPole() {
        return nomPole;
    }
    
    public List<Salarie> getLesSalaries() {
        return lesSalaries;
    }
    
    public void setCodePole(String codePole) {
        this.codePole = codePole;
    }
    
    public void setNomPole(String nomPole) {
        this.nomPole = nomPole;
    }
    
    public void setLesSalaries(List<Salarie> lesSalaries) {
        this.lesSalaries = lesSalaries;
    }
    
    
    //</editor-fold>
     
}


